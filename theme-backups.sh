#!/bin/bash

### Vars
BACKUPDIR='/home/achilles/backups/themes'
THEMEDIR='wp-content/themes'

# Get current month and week day number
date_daily=`date +"%d-%m-%Y"`
month_day=`date +"%d"`
week_day=`date +"%u"`


# it is logical to run this script daily. We take files from source folder and move them to
# appropriate destination folder

# On first month day do
if [ "$month_day" -eq 1 ] ; then
  destination=backup.monthly/$date_daily
else
  # On saturdays do
  if [ "$week_day" -eq 6 ] ; then
    destination=backup.weekly/$date_daily
  else
    # On any regular day do
    destination=backup.daily/$date_daily
  fi
fi

if [ ! -d /home/achilles/backups/files/nonwp/ ]; then
  mkdir -p /home/achilles/backups/files/nonwp/
fi

## Create Log 
if [ ! -f $BACKUPDIR/logs/all-site.log ]; then
	touch $BACKUPDIR/logs/all-site.log
fi

## Clear Log
echo "" > $BACKUPDIR/logs/all-site.log

#Get folders from webapps
array=($HOME/webapps/*)

# Loop array of folders
for dir in "${array[@]}"; do

#If WordPress
if [ -f $dir/wp-config.php ]; then

## Read Database Credentials
WPDBNAME=`cat $dir/wp-config.php | grep DB_NAME | cut -d \' -f 4`
WPDBUSER=`cat $dir/wp-config.php | grep DB_USER | cut -d \' -f 4`
WPDBPASS=`cat $dir/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4`
PREFIX=`cat $dir/wp-config.php | grep table_prefix | cut -d \' -f 2`

#Create Conf file
if [ ! -f $HOME/backups/conf/$WPDBNAME.cnf ]; then
        echo "Creating Config File"
        touch "$HOME/backups/conf/"$WPDBNAME.cnf
        #Write the credentials to conf file
        echo -e "[client]\npassword="\'$WPDBPASS\' > "$HOME/backups/conf/"$WPDBNAME.cnf
fi


# Updated : Used MySQL Native to get Site URL
URL=$(/usr/bin/mysql --defaults-file=$HOME/backups/conf/$WPDBNAME.cnf $WPDBNAME -u $WPDBUSER -se "SELECT option_value FROM ${PREFIX}options WHERE option_name=\"siteurl\"")

# Strips http// and get the remaining value for SiteURL
SITEURL="${URL##*//}"

# Create Personalized Site Folder - Theme
FOLDER_CHECK=$BACKUPDIR/$SITEURL
if [ ! -d $FOLDER_CHECK ]; then
 echo "Creating Dirctory $FOLDER_CHECK"
 mkdir -p $FOLDER_CHECK
fi

	## Check if Backup Exists - Prevent Dupe
	if [ -f $BACKUPDIR/$SITEURL/${dir##*/}-themes-$(date +%y%m%d-%H%M%S).tar.gz ]; then
		echo "Backup already Exist!"
	else
		echo "$(date +%Y-%m-%d-%H:%M:%S) - Backing up Wordpress Site: ${SITEURL%/} -  $dir..." >> $BACKUPDIR/logs/all-site.log 2>&1

		## Create Backup Dir LOGS
                if [ ! -d $BACKUPDIR/logs ]; then
                   mkdir -p $BACKUPDIR/logs
                fi
		
		## Create DEST Folder backup.daily
		if [ ! -d $BACKUPDIR/$SITEURL/$destination ]; then
		   mkdir -p $BACKUPDIR/$SITEURL/$destination
		fi

		## Begin Backup
		tar -zcvf "$BACKUPDIR/$SITEURL/$destination/${dir##*/}-themes-$(date +%y%m%d-%H%M%S).tar.gz" -C $dir/$THEMEDIR . >> $BACKUPDIR/logs/${SITEURL%/}.log 2>&1

		## If Backup Fails
		#if [ ! -f $BACKUPDIR/$SITEURL/${dir##*/}-themes-$(date +%y%m%d-%H%M%S).tar.gz ]; then
                        #ls -l $BACKUPDIR/$SITEURL/${dir##*/} | mail shiloh@shaneoquinn.com -s "Backup Script - Daily Backup Failed"
               # fi
	fi



# Move the files
#mkdir -p $BACKUPDIR/$destination
#mv -v $BACKUPDIR/${SITEURL%/}/*.gz $BACKUPDIR/$destination


fi # End If WordPress (wp-config)
done

## Send to Mail
#mail -s "Backup Done - Daily" shiloh@shaneoquinn.com < $BACKUPDIR/logs/all-site.log



# daily - keep for 14 days
#find $BACKUPDIR/backup.daily/ -maxdepth 1 -mtime +14 -type d -exec rm -rv {} \;

# weekly - keep for 60 days
#find $BACKUPDIR/backup.weekly/ -maxdepth 1 -mtime +60 -type d -exec rm -rv {} \;

# monthly - keep for 300 days
#find $BACKUPDIR/backup.monthly/ -maxdepth 1 -mtime +300 -type d -exec rm -rv {} \;

