#!/bin/bash

if [ ! -d /home/achilles/backups/files/nonwp/ ]; then
  mkdir -p /home/achilles/backups/files/nonwp/
fi


#Get folders from webapps
array=($HOME/webapps/*)

# Loop array of folders
for dir in "${array[@]}"; do

#If WordPress
if [ -f $dir/wp-config.php ]; then

#Get SiteURL using WP-CLI
#URL=`/usr/local/bin/php $HOME/wp --path=$dir option get siteurl`

# Updated : Used MySQL Native to get Site URL
WPDBNAME=`cat $dir/wp-config.php | grep DB_NAME | cut -d \' -f 4`
WPDBUSER=`cat $dir/wp-config.php | grep DB_USER | cut -d \' -f 4`
WPDBPASS=`cat $dir/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4`
PREFIX=`cat $dir/wp-config.php | grep table_prefix | cut -d \' -f 2`
URL=$(/usr/bin/mysql --defaults-file=$HOME/backups/conf/$WPDBNAME.cnf $WPDBNAME -u $WPDBUSER -se "SELECT option_value FROM ${PREFIX}options WHERE option_name=\"siteurl\"")

# Strips http// and get the remaining value for SiteURL
SITEURL="${URL##*//}"

# Create Personalized Site URL
if [ ! -d $HOME/backups/files/$SITEURL ]; then
 echo "Creating Dirctory $HOME/backups/files/$SITEURL"
 mkdir -p $HOME/backups/files/$SITEURL
fi

	if [ -f /home/achilles/backups/files/$SITEURL/${dir##*/}-full-$(date +%y%m%d).tar.gz ]; then
		echo "Backup already Exist!"
	else
		echo "Backing up Wordpress Site:  $dir..."
		tar zcf "/home/achilles/backups/files/$SITEURL/${dir##*/}-full-$(date +%y%m%d).tar.gz" $dir \
		--exclude='.git' \
		--exclude='*.zip' \
		--exclude='*.sql' \
 		--exclude="wp-content/uploads" \
		--exclude="*.gz" \
		--exclude="*.tar" \
		--exclude="*.mov"
	fi

#else # NONWP

	#if [ -f /home/achilles/backups/files/nonwp/${dir-##*/}full-$(date +%y%m%d).tar.gz ]; then
        #        echo "Backup already Exist!"
        #else
	#	echo "Backing up $dir.."
        #        tar zcf "/home/achilles/backups/files/${dir##*/}-full-$(date +%y%m%d).tar.gz" $dir --exclude='.git' --exclude='*.zip' --exclude='*.sql'
	#fi
fi # End If WordPress (wp-config)


done

