##
ssh $SERVER "cd $APPDIR \
&& if [ "${1}" == "1" ]; then php $REMOTEHOME/wp core update; fi \
&& if [ "${2}" == "1" ]; then php $REMOTEHOME/wp theme update --all; fi \
&& if [ "${3}" == "1" ]; then php $REMOTEHOME/wp plugin update --all; fi \
&& cd $GIT/ \
&& git pull origin master \
&& rsync -az $APPDIR/ . --delete --exclude=\".git\" \
--exclude=\"wp-content/uploads\" \
--exclude=\"wp-content/cache\" \
--exclude=\"wp-snapshots\" \
--exclude=\"*.MOV\" \
--exclude=\"*.sh\" \
--exclude=\"*.sql\" \
--exclude=\"*.zip\" \
--exclude=\"*.gz\" \
--exclude=\"*.tgz\" \
--exclude=\"wp-content/wflogs\" \
--exclude=\"blogs.dir\" \
--exclude=\"contests\" "

STATUS=$(ssh $SERVER "cd $GIT/ && git ls-files -dmo --exclude-standard")

if [ -z "$STATUS" ]; then
 echo "EMPTY, Nothing to commit"
else
# Having New Commits
scp $HOME/git-check/$EXECFILE $SERVER:$GIT/

FILECHECK=$(ssh $SERVER "ls -lh $GIT/$EXECFILE")
 if [[ ! -z "$FILECHECK" ]]; then
  echo "File Exists!"
 GIT_EXEC=$(ssh $SERVER "cd $GIT && sh $GIT/$EXECFILE")
 if [[ ! -z "$GIT_EXEC" ]]; then
  echo $GIT_EXEC
  ssh $SERVER "rm $GIT/$EXECFILE"
  echo "Removed SH File"
 fi

 fi
fi
