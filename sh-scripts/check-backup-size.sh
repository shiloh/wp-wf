#!/bin/bash

array_sites=(web540 web541 web548 web552 web553 web554 cloud1 cloud2 cloud3 cloud4)
for sites in "${array_sites[@]}"; do
 echo "=============== $sites =====================" 
 ssh $sites "du -sh /home/achilles/backups"
done
