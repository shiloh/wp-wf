#!/bin/sh
#REPO

mkdir -p $HOME/git/$1.git
#cd $HOME/git/$1.git
git init --bare $HOME/git/$1.git

touch $HOME/git/$1.git/hooks/post-receive

echo "#!/bin/sh" >> $HOME/git/$1.git/hooks/post-receive
echo "GIT_WORK_TREE=$2 git checkout -f" >> $HOME/git/$1.git/hooks/post-receive

chmod +x $HOME/git/$1.git/hooks/post-receive

