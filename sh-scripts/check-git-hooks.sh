#!/bin/bash
# Author Shiloh Impang 
# Email : shiloh@shaneoquinn.com
# This script checks the GIT hooks/post-receive WORK TREE
#


#  ---- START  ----
# Array Servers / from ~/.ssh/config
array_sites=(web540 web541 web548 web552 web553 web554 cloud1 cloud2 cloud3 cloud4)
for sites in "${array_sites[@]}"; do
 echo ""
# echo "$sites: ===========> WEBAPP NAME   -    GIT NAME <============="
printf "$sites: %-32s Webapp Name\n" "GIT Name"
echo "=============================================================================="
repos=($(ssh $sites "grep -R checkout /home/achilles/git/*/hooks/post-re*" | sed 's/ //g' | sed 's/-f/ /g' ))

# Loop GIT Repo
for repo in "${repos[@]}"; do
 
 var1=$(echo $repo | sed 's/\// /g' | sed 's/gitcheckout//g' | awk '{print $4}')   
 var2=$(echo $repo | sed 's/\// /g' | sed 's/gitcheckout//g' | awk '{print $10}')

if [ ! -z "$var2" ]; then
  #echo "=====> " $var2 " -- " $var1
  printf "%-40s $var2\n" "$var1"
elif [ -z "$var1" ]  && [ -z "$var2" ]; then
  echo "xxxx <== NO GIT and Webapps"
else
  echo "xxxx <== NO Webapps: $var1"
fi

done

done
