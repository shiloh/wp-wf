#!/bin/bash
#If WordPress

#Get folders from webapps
array=($HOME/webapps/*)

# Loop array of folders
for sites in "${array[@]}"; do
if [ -f $sites/wp-config.php ]; then

#Get Database Name, user and password and contactenate
WPDBNAME=`cat $sites/wp-config.php | grep DB_NAME | cut -d \' -f 4`
WPDBUSER=`cat $sites/wp-config.php | grep DB_USER | cut -d \' -f 4`
WPDBPASS=`cat $sites/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4`
PREFIX=`cat $sites/wp-config.php | grep table_prefix | cut -d \' -f 2`
DBHOST=`cat $sites/wp-config.php | grep DB_HOST | cut -d \' -f 4`

#test=$(ssh cloud1 "cat /home/achilles/webapps/tourismtalk/wp-config.php | grep DB_HOST | cut -d \' -f 4")
HOST=$(echo $DBHOST | sed 's/:/ /g' | awk '{print $1}')
PORT=$(echo $DBHOST | sed 's/:/ /g' | awk '{print $2}')

if [ -z $PORT ]; then
  PORT=3306
fi


URL=$(/usr/bin/mysql --defaults-file=$HOME/backups/conf/$WPDBNAME.cnf $WPDBNAME -u $WPDBUSER -h $HOST --port $PORT -se "SELECT option_value FROM ${PREFIX}options WHERE option_name=\"siteurl\"")
# Strips http// and get the remaining value for SiteURL
SITEURL="${URL##*//}"

echo $SITEURL
fi
done
