test=$(ssh cloud1 "cat /home/achilles/webapps/rahalhairtransplant/wp-config.php | grep DB_HOST | cut -d \' -f 4")
host=$(echo $test | sed 's/:/ /g' | awk '{print $1}')
port=$(echo $test | sed 's/:/ /g' | awk '{print $2}')

echo "local" $host 
echo "port" $port
