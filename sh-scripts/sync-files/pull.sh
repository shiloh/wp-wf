SERVER=$(hostname --ip-address)
APP_PATH=$PWD
rsync -avz achilles@$SERVER:$APP_PATH . --exclude=".git" --delete
