#!/bin/bash
#If WordPress

#Get folders from webapps
array=($HOME/webapps/*)

# Loop array of folders
for sites in "${array[@]}"; do
if [ -f $sites/wp-config.php ]; then

#Get Database Name, user and password and contactenate
WPDBNAME=`cat $sites/wp-config.php | grep DB_NAME | cut -d \' -f 4`
WPDBUSER=`cat $sites/wp-config.php | grep DB_USER | cut -d \' -f 4`
WPDBPASS=`cat $sites/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4`
PREFIX=`cat $sites/wp-config.php | grep table_prefix | cut -d \' -f 2`

site="$(/usr/local/bin/php /home/achilles/wp --path=$sites option get siteurl)"
stat="$(/usr/local/bin/php /home/achilles/wp --path=$sites core check-update --major)"



if [ "$stat" != "Success: WordPress is at the latest version." ]; then

  

#echo "Do you wish to update the WordPress Version?"
# select yn in "Yes" "No"; do 
#  case $yn in       
#       Yes ) /usr/local/bin/php /home/achilles/wp  --path=$sites core info; break;;
#       No ) break;;
#  esac
#done
   
echo $site 
echo $stat | awk '{print "Needs Update: " $4}'
echo "Path : " $sites
echo "Code: ssh $1 \"php /home/achilles/wp --path=\"$sites\" core update\""
echo " ------------- " 

fi

fi
done
