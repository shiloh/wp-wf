#!/bin/bash

#Get folders from webapps
array=($HOME/webapps/*)

CONFDIR="$HOME/backups/conf/"
#Create Conf Folder
if [ ! -d "$CONFDIR" ]; then 
	mkdir -p "$CONFDIR"
	echo "Creating $CONFDIR"
else
	echo "$CONFDIR already Exists!"
fi

# Loop array of folders
for dir in "${array[@]}"; do
     
#If WordPress   
if [ -f $dir/wp-config.php ]; then

#Get Database Name, user and password and contactenate
WPDBNAME=`cat $dir/wp-config.php | grep DB_NAME | cut -d \' -f 4`
WPDBUSER=`cat $dir/wp-config.php | grep DB_USER | cut -d \' -f 4`
WPDBPASS=`cat $dir/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4`
PREFIX=`cat $dir/wp-config.php | grep table_prefix | cut -d \' -f 2`


#Create Conf file
if [ ! -f $HOME/backups/conf/$WPDBNAME.cnf ]; then
	echo "Creating Config File"
	touch "$HOME/backups/conf/"$WPDBNAME.cnf
	#Write the credentials to conf file
	echo -e "[client]\npassword="\'$WPDBPASS\' > "$HOME/backups/conf/"$WPDBNAME.cnf
else
	echo "Config File Exists! => $CONFDIR$WPDBNAME.cnf"
fi

# Dump SQL

# Get Site URL using WP-CLI : Note: Specify WP-CLI Path
#URL=`/usr/local/bin/php $HOME/wp --path=$dir option get siteurl`
URL=$(/usr/bin/mysql --defaults-file=$HOME/backups/conf/$WPDBNAME.cnf $WPDBNAME -u $WPDBUSER -se "SELECT option_value FROM ${PREFIX}options WHERE option_name=\"siteurl\"")



# Strips http// and get the remaining value for SiteURL
SITEURL="${URL##*//}"

# Create Personalized Site URL
if [ ! -d $HOME/backups/database/$SITEURL ]; then
	echo "Create Site Folder"
	mkdir -p "$HOME/backups/database/$SITEURL"
else
	echo "Site Folder Already Exists! => $HOME/backups/database/$SITEURL"
fi

if [ -f "$HOME/backups/conf/$WPDBNAME.cnf" ]; then
# Backup database then GZIP
echo "Executing Backups..."

if [ -f $HOME/backups/database/$SITEURL/$WPDBNAME-$(date +\%Y\%m\%d).sql.gz ]; then
	echo "Backup Already Exists! => $HOME/backups/database/$SITEURL/$WPDBNAME-$(date +\%Y\%m\%d).sql.gz" 

else

echo "Backup Database ..."

/usr/bin/mysqldump --defaults-file=$HOME/backups/conf/$WPDBNAME.cnf -u $WPDBUSER $WPDBNAME | gzip -9 > $HOME/backups/database/$SITEURL/$WPDBNAME-$(date +\%Y\%m\%d).sql.gz 2>> $HOME/logs/user/$WPDBNAME_cron.log


#Check backup File
if [ -f $HOME/backups/database/$SITEURL/$WPDBNAME-$(date +\%Y\%m\%d).sql.gz ]; then
	echo "Backup Finished and Verified"
fi # End check config file

fi # End Chec if File Exist before RUN backup

fi # End Detect Config File

#add else here for non WP


fi # End If WordPress (wp-config)


done #End For Loop
