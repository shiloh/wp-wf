#!/bin/bash

BACKUPDIR=$HOME/backups/incremental
EXCLUDE_TXT=$HOME/wp-wf/exclude.txt
SYNC_SCRIPT=$HOME/wp-wf/rsync-time-backup/rsync_tmbackup.sh
CONFDIR=$HOME/backups/conf


#Get folders from webapps
array=($HOME/webapps/*)


#Create Conf Folder
if [ ! -d "$CONFDIR" ]; then
        mkdir -p "$CONFDIR"
        echo "Creating $CONFDIR"
fi

# Loop array of folders
for dir in "${array[@]}"; do

#If WordPress
if [ -f $dir/wp-config.php ]; then

#Get SiteURL using WP-CLI
#URL=`/usr/local/bin/php $HOME/wp --path=$dir option get siteurl`

# Updated : Used MySQL Native to get Site URL
WPDBNAME=`cat $dir/wp-config.php | grep DB_NAME | cut -d \' -f 4`
WPDBUSER=`cat $dir/wp-config.php | grep DB_USER | cut -d \' -f 4`
WPDBPASS=`cat $dir/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4`
PREFIX=`cat $dir/wp-config.php | grep table_prefix | cut -d \' -f 2`
URL=$(/usr/bin/mysql --defaults-file=$CONFDIR/$WPDBNAME.cnf $WPDBNAME -u $WPDBUSER -se "SELECT option_value FROM ${PREFIX}options WHERE option_name=\"siteurl\"")

#Create Conf file
if [ ! -f $CONFDIR/$WPDBNAME.cnf ]; then
        echo "Creating Config File"
        touch $CONFDIR/$WPDBNAME.cnf
        echo -e "[client]\npassword="\'$WPDBPASS\' > $CONFDIR/$WPDBNAME.cnf
fi

# Strips http// and get the remaining value for SiteURL
SITEURL="${URL##*//}"

if [ ! -d $BACKUPDIR/$SITEURL ]; then
   mkdir -p -- "$BACKUPDIR/$SITEURL"
   if [ ! -f $BACKUPDIR/$SITEURL/backup.marker ]; then
     touch "$BACKUPDIR/$SITEURL/backup.marker"
   fi
fi

#mkdir -p -- "$BACKUPDIR/$SITEURL" ; touch "$BACKUPDIR/$SITEURL/backup.marker"

sh $SYNC_SCRIPT $dir $BACKUPDIR/$SITEURL $EXCLUDE_TXT

fi # End If WordPress (wp-config)

done
